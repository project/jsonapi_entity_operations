<?php

namespace Drupal\jsonapi_entity_operations\Form;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure JSON:API Entity Operations settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jsonapi_entity_operations_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jsonapi_entity_operations.settings'];
  }

  private function getEntityTypes() {
    $options = [];
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();

    foreach ($entity_types as $entity_type_id => $entity_type) {
      // Only if content entity type has bundles.
      if ($entity_type->entityClassImplements('Drupal\Core\Entity\ContentEntityInterface')
        && $entity_type->hasKey('bundle')) {

        // Get the bundles who belongs to this entity type.
        $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
        if ($bundles) {
          $keys = array_keys($bundles);
          $bundle = reset($keys);

          // Check if exist a JSON:API route for this entity type.
          $route = "jsonapi.{$entity_type_id}--{$bundle}.collection";
          $url = Url::fromRoute($route);
          if ($url->access()) {
            $options[$entity_type_id] = $entity_type->getLabel();
          }
        }
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed entity types to enable JSON:API operations'),
      '#description' =>$this->t('Select which entity types you want to enable JSON:API operations.'),
      '#options' => $this->getEntityTypes(),
      '#default_value' => $this->config('jsonapi_entity_operations.settings')->get('entity_types'),
      '#size' => 6,
      '#multiple' => TRUE,
    ];



    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $allowedEntityTypes = array_keys(array_filter($values['entity_types']));
    $this->config('jsonapi_entity_operations.settings')
      ->set('entity_types', $allowedEntityTypes)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
